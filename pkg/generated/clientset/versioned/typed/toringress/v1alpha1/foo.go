/*
Copyright The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by client-gen. DO NOT EDIT.

package v1alpha1

import (
	"time"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
	v1alpha1 "github.com/einyx/tor-ingress-controller/pkg/apis/toringress/v1alpha1"
	scheme "github.com/einyx/tor-ingress-controller/pkg/generated/clientset/versioned/scheme"
)

// TorsGetter has a method to return a TorInterface.
// A group's client should implement this interface.
type TorsGetter interface {
	Tors(namespace string) TorInterface
}

// TorInterface has methods to work with Tor resources.
type TorInterface interface {
	Create(*v1alpha1.Tor) (*v1alpha1.Tor, error)
	Update(*v1alpha1.Tor) (*v1alpha1.Tor, error)
	UpdateStatus(*v1alpha1.Tor) (*v1alpha1.Tor, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*v1alpha1.Tor, error)
	List(opts v1.ListOptions) (*v1alpha1.TorList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.Tor, err error)
	TorExpansion
}

// tors implements TorInterface
type tors struct {
	client rest.Interface
	ns     string
}

// newTors returns a Tors
func newTors(c *ToringressV1alpha1Client, namespace string) *tors {
	return &tors{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the tor, and returns the corresponding tor object, and an error if there is any.
func (c *tors) Get(name string, options v1.GetOptions) (result *v1alpha1.Tor, err error) {
	result = &v1alpha1.Tor{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("tors").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of Tors that match those selectors.
func (c *tors) List(opts v1.ListOptions) (result *v1alpha1.TorList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1alpha1.TorList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("tors").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested tors.
func (c *tors) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("tors").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a tor and creates it.  Returns the server's representation of the tor, and an error, if there is any.
func (c *tors) Create(tor *v1alpha1.Tor) (result *v1alpha1.Tor, err error) {
	result = &v1alpha1.Tor{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("tors").
		Body(tor).
		Do().
		Into(result)
	return
}

// Update takes the representation of a tor and updates it. Returns the server's representation of the tor, and an error, if there is any.
func (c *tors) Update(tor *v1alpha1.Tor) (result *v1alpha1.Tor, err error) {
	result = &v1alpha1.Tor{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("tors").
		Name(tor.Name).
		Body(tor).
		Do().
		Into(result)
	return
}

// UpdateStatus was generated because the type contains a Status member.
// Add a +genclient:noStatus comment above the type to avoid generating UpdateStatus().

func (c *tors) UpdateStatus(tor *v1alpha1.Tor) (result *v1alpha1.Tor, err error) {
	result = &v1alpha1.Tor{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("tors").
		Name(tor.Name).
		SubResource("status").
		Body(tor).
		Do().
		Into(result)
	return
}

// Delete takes name of the tor and deletes it. Returns an error if one occurs.
func (c *tors) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("tors").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *tors) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("tors").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched tor.
func (c *tors) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.Tor, err error) {
	result = &v1alpha1.Tor{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("tors").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
